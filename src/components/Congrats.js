import React from "react";

const Congrats = (props) => {
    return (
        <div id="congrats-alert" style ={{border:"1px solid", backgroundColor:'green' ,color:'white',padding:"10px", borderRadius:'3px' , opacity:'0.4'}} >
            Congratulations! You guessed the word!
        </div>
    );
};

export default Congrats