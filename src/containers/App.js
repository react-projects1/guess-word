import React, {Component} from 'react';
import Congrats from "../components/Congrats";
import GuessedWords from "../components/GuessedWords";
import Input from "../containers/Input";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            success: false,
            secretWord: "",
            guessedWords: []
        }
        this.getLetterMatchCount = this.getLetterMatchCount.bind(this);
    }
    getLetterMatchCount(guessedWord,secretWord){
            let similar =[];
            let nums;
            for (let index = 0; index < guessedWord.length; index++) {
                if(secretWord.includes(guessedWord[index])){
                   similar.push(guessedWord[index]) ;
                }   
            }
            nums = similar.length;
            this.setState({
                guessedWords:[
                    ...this.state.guessedWords,
                    {
                        guessedWord: guessedWord,
                        letterMatchCount :nums
                    }
                ]
            })
    };
   

    componentDidMount() {
        this.setSecretWord("Letter");
    };

    setSecretWord = (secretWord) => {
        this.setState({
            secretWord:secretWord
        })
    };
    guessWord = (guessedWord) => {
        if(guessedWord === this.state.secretWord){
            this.setState({success:true});
            this.getLetterMatchCount(guessedWord,this.state.secretWord);
        }else{
            this.getLetterMatchCount(guessedWord,this.state.secretWord);
        }
    };
   


    render() {
        
        return (
           <div className={"container"}>
               <h2 className={"mt-2"}>Guess the word!</h2>
               {this.state.success ?
               <Congrats success={false}/>
               :
               <Input guessWord={this.guessWord}/>
                }
               <GuessedWords guessedWords={this.state.guessedWords} />
           </div>
        );
    }
}

export default App;
