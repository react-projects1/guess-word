import React, {Component, Fragment} from "react";


class Input extends Component {

    constructor(props) {
        super(props);
        this.state = {
            guessWord: ""
        }
    }

    onChangeGuessedWord = (e) => {
        this.setState({
            guessWord:e.target.value
        })
    }
    onSubmitGuessedWord = (e) => {
        e.preventDefault();
        if(this.state.guessWord.length){
            this.props.guessWord(this.state.guessWord);
        }

        this.setState({
            guessWord:""
        })
    };

    render() {
        let contents = this.props.success
            ? null
            : (
                <Fragment>
                    <input 
                        id={"word-guess"}
                        placeholder={"enter guess"}
                        className={"mb-2 mx-sm-3"}
                        value = {this.state.guessWord}
                        onChange = {this.onChangeGuessedWord.bind(this)}
                    />
                    <button className={"btn btn-primary mb-2"} id={"guess-word-button"}>Submit</button>
                </Fragment>
            );
        return (
            <form onSubmit={this.onSubmitGuessedWord}>
                {contents}
            </form>
        )
    }
};

export default Input;